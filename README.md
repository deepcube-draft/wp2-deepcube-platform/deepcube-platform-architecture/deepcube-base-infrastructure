# DeepCube Base Infrastructure


This repository is the entrypoint of any DeepCube platform installation.
It lists all the required operations needed to setup a DeepCube platform from scracth using Terraform, Ansible as automation tool.

### I. Base Infrastructure Installation 
The resources used to setup the base infrastructure are described in this repository.

#### Steps

0. `cd terraform`
1. Copy `openrc.sample.sh` as `openrc.sh` and edit it with your OpenStack provider
2. bash `source ./openrc.sh` or fish `source ./openrc.fish`
3. Eventually edit `00-variables.tf`
3. `terraform init`
4. `terraform refresh`
5. `terraform plan`
6. `terraform apply`

### II. Docker Swarm Installation

WIP

### III. HopsWorks Installation

WIP