# list of computed locals


locals  {
    OS_CLUSTER_REGIONS = var.NETWORK_PROVIDER_TYPE == "openstack" ? var.CLUSTER_REGIONS : []
    OVH_CLUSTER_REGIONS = var.NETWORK_PROVIDER_TYPE == "ovh" ? var.CLUSTER_REGIONS : []
}