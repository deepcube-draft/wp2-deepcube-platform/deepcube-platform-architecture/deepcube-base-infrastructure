# due to impossibility to use variables in terraform configuration, we rely on jinja to generate this file
# https://github.com/hashicorp/terraform/issues/13022
terraform {
  required_version = ">= v1.0.0"

  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }

  # State storage
  backend "swift" {
    container         = "${OS_TF_STATE_SWIFT_CONTAINER}"
    archive_container = "${OS_TF_STATE_SWIFT_ARCHIVE_CONTAINER}"
    region_name       = "${OS_TF_STATE_SWIFT_REGION_NAME}"
  }

}